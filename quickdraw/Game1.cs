﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using System;

namespace quickdraw
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Texture2D buttonTexture;
        SpriteFont gameFont;
        SoundEffect clickSFX;
        SoundEffect clickNowSFX;
        Song GameMusic;

        MouseState previousState;

        bool playing = false;
        bool timeRand = false;
        bool randTimeEnd = false;
        bool reflexClick = false;
        bool breakStart = false;
        

        Random rand = new Random();
        float randTimer;
        float reflexTimer;
        float breakTime = 0f;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            //load button texture
            buttonTexture = Content.Load<Texture2D>("graphics/button");

            //load sprite font
            gameFont = Content.Load<SpriteFont>("fonts/mainSpriteFont");

            // load sound
            clickSFX = Content.Load<SoundEffect>("audio/buttonClick");
            clickNowSFX = Content.Load<SoundEffect>("audio/gameOver");

            GameMusic = Content.Load<Song>("audio/music");

            //start background music
            MediaPlayer.Play(GameMusic);
            MediaPlayer.IsRepeating = true;


            //see mouse
            IsMouseVisible = true;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            if (timeRand == false)
            {
                randTimer = rand.Next(3, 11);
                timeRand = true;
                
            }

            MouseState currentState = Mouse.GetState();

            Vector2 screenCentre = new Vector2
                (Window.ClientBounds.Width / 2,
                Window.ClientBounds.Height / 2);

            Rectangle buttonRect = new Rectangle
                ((int)screenCentre.X - buttonTexture.Width / 2,
                (int)screenCentre.Y - buttonTexture.Height / 2,
                buttonTexture.Width, buttonTexture.Height);

            if (currentState.LeftButton == ButtonState.Pressed &&
                previousState.LeftButton != ButtonState.Pressed &&
                buttonRect.Contains(currentState.X, currentState.Y))
            {
                clickSFX.Play();
                if (playing == false)
                {
                    playing = true;
                    reflexTimer = 0;
                }

            }

            if (playing == true)
            {
                randTimer -= (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (currentState.LeftButton == ButtonState.Pressed &&
                previousState.LeftButton != ButtonState.Pressed &&
                buttonRect.Contains(currentState.X, currentState.Y)&&
                randTimer <= 0 &&
                reflexClick == false)
                {
                    reflexClick = true;
                    breakStart = true;
                    clickSFX.Play();
                    breakTime = 3;
                }

                if (randTimer <= 0 && reflexClick == false)
                {
                    if (randTimeEnd == false )
                    {
                        clickNowSFX.Play();
                    }
                    randTimeEnd = true;
                    reflexTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;
                }
            }

            if (breakStart == true)
            {
                breakTime -= (float)gameTime.ElapsedGameTime.TotalSeconds;
            }

            if (currentState.LeftButton == ButtonState.Pressed &&
               previousState.LeftButton != ButtonState.Pressed &&
               buttonRect.Contains(currentState.X, currentState.Y) &&
               randTimer <= 0 &&
               reflexClick == true &&
               breakTime <= 0)
            {
                playing = false;
                timeRand = false;
                randTimeEnd = false;
                reflexClick = false;
                breakStart = false;
            }



                previousState = currentState;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();

            //center of the screen
            Vector2 screenCentre = new Vector2
                (Window.ClientBounds.Width / 2,
                Window.ClientBounds.Height / 2);

            //draw the button
            spriteBatch.Draw(buttonTexture, new Rectangle
                    ((int)screenCentre.X - buttonTexture.Width / 2,
                    (int)screenCentre.Y - buttonTexture.Height / 2,
                    buttonTexture.Width, buttonTexture.Height), Color.LightBlue);

            //text

            string prompString = "Click the button to start!";
            if(playing)
            {
                prompString = "Get ready to click!";
            }
            if(randTimeEnd)
            {
                prompString = "CLICK NOW!";
            }
            if(reflexClick)
            {
                prompString = "Click to restart game";
            }
            

            Vector2 halfTitleSize = gameFont.MeasureString("Quick Draw");
            halfTitleSize.X = (int)(halfTitleSize.X / 2);
            halfTitleSize.Y = (int)(halfTitleSize.Y / 2);

            Vector2 authorSize = gameFont.MeasureString("By Zak Mitchell");

            Vector2 halfPrompSize = gameFont.MeasureString(prompString);
            halfPrompSize.X = (int)(halfPrompSize.X / 2);
            halfPrompSize.Y = (int)(halfPrompSize.Y / 2);

            Vector2 reflexTimeSize = gameFont.MeasureString("Your reflex time is");
            Vector2 reflexNumSize = gameFont.MeasureString("10.000000");

            spriteBatch.DrawString(gameFont, "Quick Draw", screenCentre - new Vector2(0, 100) - halfTitleSize, Color.White);
            spriteBatch.DrawString(gameFont, "By Zak Mitchell", screenCentre - new Vector2(0, 80) - authorSize / 2, Color.White);
            spriteBatch.DrawString(gameFont, prompString, screenCentre - new Vector2(0, 60) - halfPrompSize, Color.White);

            if (reflexClick)
            {
                spriteBatch.DrawString(gameFont, "Your reflex time is", screenCentre - new Vector2(0, 150) - reflexTimeSize/2, Color.White);
                spriteBatch.DrawString(gameFont, reflexTimer.ToString(), screenCentre - new Vector2(0, 130) - reflexNumSize/2, Color.White);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
